# Framework for visualizing graphs (especially in the field of static analysis)
## Set-up
This project uses sbt ( [http://www.scala-sbt.org]() ) as build-tool and I therefore recommend to use sbt to get this project working with your project.
### Set-up if your project uses sbt (or Maven)
After cloning the project run sbt with:

    sbt compile publishLocal

and add to your dependencies (example in sbt-style):

    libraryDependencies += "de.tu-darmstadt" %% "graphvisualizer" % "0.1-SNAPSHOT"
### Set-up otherwise
After cloning the repository run sbt with:

    sbt compile packageBin

and copy the library to your project