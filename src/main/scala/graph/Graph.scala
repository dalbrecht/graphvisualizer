/* Copyright 2014 Dennis Albrecht
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package graph

import java.{ util => ju }

import scala.collection.JavaConversions.seqAsJavaList

import content.Edge
import content.Node
import model.Grid
import style.EdgeStyle
import util.BiDiMap
import util.PartiallySynchronizedList

class Graph[Row, Col](val name: String) {
  val grid: Grid[Row, Col] = new Grid()
  private val _path = new PathFinder[Row, Col](this, grid)
  private val _rows = new PartiallySynchronizedList[Row]
  private val _cols = new PartiallySynchronizedList[Col]
  private val _nodes = new BiDiMap[Node[Row, Col], (Row, Col)]

  def rows(): List[Row] = _rows.toList
  def columns(): List[Col] = _cols.toList
  def getRows(): ju.List[Row] = new ju.ArrayList(rows)
  def getColumns(): ju.List[Col] = new ju.ArrayList(columns)
  def lastRow(): Row = _rows.lastOption.getOrElse(throw new IllegalStateException("no rows added yet"))
  def lastColumn(): Col = _cols.lastOption.getOrElse(throw new IllegalStateException("no columns added yet"))
  def set(node: Node[Row, Col], row: Row, col: Col): Unit = {
    if (_nodes.key(node))
      throw new IllegalArgumentException("node is already added to this graph")
    synchronized {
      if (!_rows.contains(row)) {
        _rows.add(row, () => grid.addRow(row))
      }
      if (!_cols.contains(col)) {
        _cols.add(col, () => grid.addCol(col))
      }
      node.setPosition(Some(this), Some(row), Some(col))
      if (_nodes.value((row, col))) {
        val old = _nodes.value2key((row, col))
        val (from, to) = old.edges.partition(_.from == old)
        from.foreach(e => {
          e.from = node
          old.removeEdge(e)
          node.addEdge(e)
        })
        to.foreach(e => {
          e.to = node
          old.removeEdge(e)
          node.addEdge(e)
        })
        old.setPosition()
      }
      _nodes.put(node, (row, col))
      grid.setNode(row, col, node)
    }
  }
  def get(row: Row, col: Col): Node[Row, Col] = _nodes.value2key((row, col))
  def addEdge(from: Node[Row, Col], to: Node[Row, Col], style: EdgeStyle): Edge[Row, Col] = {
    if (!_nodes.key(from))
      throw new IllegalArgumentException("given from-node is not part of this graph")
    if (!_nodes.key(to))
      throw new IllegalArgumentException("given to-node is not part of this graph")
    if (from == to)
      throw new IllegalArgumentException("no edges from and to the same node supported")
    synchronized {
      val path = _path.find(from, to)
      val edge = Edge(from, to, style, path._1, path._2, path._3)
      from.addEdge(edge)
      to.addEdge(edge)
      grid.addEdge(edge)
      edge
    }
  }
  def addEdge(fromRow: Row, fromCol: Col, toRow: Row, toCol: Col, style: EdgeStyle): Edge[Row, Col] = {
    if (!_nodes.value((fromRow, fromCol)))
      throw new IllegalArgumentException("given from-coordinates are not associated with a node")
    if (!_nodes.value((toRow, toCol)))
      throw new IllegalArgumentException("given to-coordinates are not associated with a node")
    addEdge(get(fromRow, fromCol), get(toRow, toCol), style)
  }
  def addSubGraph[SubRowT, SubColT](root: Node[Row, Col], name: String): Graph[SubRowT, SubColT] = {
    if (!_nodes.key(root))
      throw new IllegalArgumentException("given node is not part of this graph")
    root.synchronized {
      if (root.getSubgraph != null)
        throw new IllegalStateException("given node already defines some subgraph")
      val subgraph = new Graph[SubRowT, SubColT](name)
      root.setSubgraph(subgraph)
      subgraph
    }
  }
}