/* Copyright 2014 Dennis Albrecht
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package graph
package style

import java.awt.Color

final class EdgeStyle private[style] (val size: Int, val color: Color, val dashed: Boolean = false)

object EdgeStyle {
  def small(): EdgeStyle = small(Color.BLACK)
  def small(color: Color): EdgeStyle = new EdgeStyle(1, color)
  def medium(): EdgeStyle = medium(Color.BLACK)
  def medium(color: Color): EdgeStyle = new EdgeStyle(2, color)
  def large(): EdgeStyle = large(Color.BLACK)
  def large(color: Color): EdgeStyle = new EdgeStyle(3, color)
  def smallDashed(): EdgeStyle = smallDashed(Color.BLACK)
  def smallDashed(color: Color): EdgeStyle = new EdgeStyle(1, color, true)
  def mediumDashed(): EdgeStyle = mediumDashed(Color.BLACK)
  def mediumDashed(color: Color): EdgeStyle = new EdgeStyle(2, color, true)
  def largeDashed(): EdgeStyle = largeDashed(Color.BLACK)
  def largeDashed(color: Color): EdgeStyle = new EdgeStyle(3, color, true)
}