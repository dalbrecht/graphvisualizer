/* Copyright 2014 Dennis Albrecht
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package graph

import content.Node
import model.Grid

private[graph] class PathFinder[R, C](graph: Graph[R, C], grid: Grid[R, C]) {
  def find(from: Node[R, C], to: Node[R, C]): (Seq[((Option[R], Int), (Option[C], Int))], Option[(Option[R], Int)], Option[(Option[C], Int)]) = {
    val rows = None :: graph.rows.map(Some(_))
    val cols = None :: graph.columns.map(Some(_))
    val fromRow = Some(from.getRow)
    val fromCol = Some(from.getColumn)
    val toRow = Some(to.getRow)
    val toCol = Some(to.getColumn)
    def predRow(row: Option[R]): Option[R] = rows(rows.indexOf(row) - 1)
    def predCol(col: Option[C]): Option[C] = cols(cols.indexOf(col) - 1)
    def subRow(row: Option[R]): (Option[R], Int) = grid.newSubRow(row)
    def subCol(col: Option[C]): (Option[C], Int) = grid.newSubCol(col)
    val rowDiff = rows.indexOf(toRow) - rows.indexOf(fromRow)
    val colDiff = cols.indexOf(toCol) - cols.indexOf(fromCol)
    (rowDiff, colDiff) match {
      case (0, 1) | (1, 0) => {
        (Seq(((fromRow, 0), (fromCol, 0)),
          ((toRow, 0), (toCol, 0))),
          None, None)
      }
      case (r, 1) => {
        val fromSubCol = subCol(fromCol)
        (Seq(((fromRow, 0), (fromCol, 0)),
          ((fromRow, 0), fromSubCol),
          ((toRow, 0), fromSubCol),
          ((toRow, 0), (toCol, 0))),
          None, Some(fromSubCol))
      }
      case (1, c) => {
        val fromSubRow = subRow(fromRow)
        (Seq(((fromRow, 0), (fromCol, 0)),
          (fromSubRow, (fromCol, 0)),
          (fromSubRow, (toCol, 0)),
          ((toRow, 0), (toCol, 0))),
          Some(fromSubRow), None)
      }
      case (0, c) => {
        val fromSubRow = subRow(fromRow)
        val predToSubCol = subCol(predCol(toCol))
        (Seq(((fromRow, 0), (fromCol, 0)),
          (fromSubRow, (fromCol, 0)),
          (fromSubRow, predToSubCol),
          ((toRow, 0), predToSubCol),
          ((toRow, 0), (toCol, 0))),
          Some(fromSubRow), Some(predToSubCol))
      }
      case (r, 0) => {
        val fromSubCol = subCol(fromCol)
        val predToSubRow = subRow(predRow(toRow))
        (Seq(((fromRow, 0), (fromCol, 0)),
          ((fromRow, 0), fromSubCol),
          (predToSubRow, fromSubCol),
          (predToSubRow, (toCol, 0)),
          ((toRow, 0), (toCol, 0))),
          Some(predToSubRow), Some(fromSubCol))
      }
      case _ => {
        val fromSubCol = subCol(fromCol)
        val predToSubRow = subRow(predRow(toRow))
        (Seq(((fromRow, 0), (fromCol, 0)),
          ((fromRow, 0), fromSubCol),
          (predToSubRow, fromSubCol),
          (predToSubRow, (toCol, 0)),
          ((toRow, 0), (toCol, 0))),
          Some(predToSubRow), Some(fromSubCol))
      }
    }
  }
}