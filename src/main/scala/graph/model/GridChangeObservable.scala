/* Copyright 2014 Dennis Albrecht
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package graph
package model

import scala.collection.mutable.Buffer

import util.Observable
import util.Observer

sealed trait GridChange[R, C]
case class RowAddition[R, C](index: Int, r: Option[R], sub: Int) extends GridChange[R, C]
case class ColumnAddition[R, C](index: Int, c: Option[C], sub: Int) extends GridChange[R, C]
case class CellUpdate[R, C](r: Option[R], subR: Int, c: Option[C], subC: Int, newVal: Cell[R, C]) extends GridChange[R, C]

trait GridChangeObservable[R, C] extends Observable[GridChange[R, C]] {
  private val _observer: Buffer[Observer[_ >: GridChange[R, C]]] = Buffer()
  def register(observer: Observer[_ >: GridChange[R, C]]): Unit = {
    _observer.synchronized { _observer += observer }
  }
  def unregister(observer: Observer[_ >: GridChange[R, C]]): Unit = {
    _observer.synchronized { _observer -= observer }
  }
  protected def fire(change: GridChange[R, C]): Unit = {
    _observer.synchronized { _observer.foreach(_.update(this, change)) }
  }
}

trait GridChangeRelay[R, C] extends GridChangeObservable[R, C] with Observer[GridChange[R, C]] {
  def update(observable: Observable[GridChange[R, C]], change: GridChange[R, C]): Unit = {
    fire(change)
  }
}