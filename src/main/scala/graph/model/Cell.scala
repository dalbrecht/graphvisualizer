/* Copyright 2014 Dennis Albrecht
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package graph
package model

import content.Edge
import content.Node

private[model] object Cell {
  def toN[R, C](edge: Option[SingleUnitCell[R, C]], checked: Boolean): Option[VStraightCell[R, C]] = {
    edge.fold[Option[VStraightCell[R, C]]](None)(_.toN(checked))
  }
  def toE[R, C](edge: Option[SingleUnitCell[R, C]], checked: Boolean): Option[HStraightCell[R, C]] = {
    edge.fold[Option[HStraightCell[R, C]]](None)(_.toE(checked))
  }
  def toW[R, C](edge: Option[SingleUnitCell[R, C]], checked: Boolean): Option[HStraightCell[R, C]] = {
    edge.fold[Option[HStraightCell[R, C]]](None)(_.toW(checked))
  }
  def toS[R, C](edge: Option[SingleUnitCell[R, C]], checked: Boolean): Option[VStraightCell[R, C]] = {
    edge.fold[Option[VStraightCell[R, C]]](None)(_.toS(checked))
  }
}

sealed trait Cell[R, C]
final case class EmptyCell[R, C]() extends Cell[R, C]
final case class NodeCell[R, C](node: Node[R, C]) extends Cell[R, C]
sealed trait EdgeCell[R, C] extends Cell[R, C] {
  def toN(checked: Boolean): Option[VStraightCell[R, C]] = None
  def toE(checked: Boolean): Option[HStraightCell[R, C]] = None
  def toW(checked: Boolean): Option[HStraightCell[R, C]] = None
  def toS(checked: Boolean): Option[VStraightCell[R, C]] = None
}
final case class MultiUnitCell[R, C](edges: Seq[Option[SingleUnitCell[R, C]]]) extends EdgeCell[R, C] {
  override def toN(checked: Boolean) = Cell.toN(edges.head, checked)
  override def toE(checked: Boolean) = Cell.toE(edges.last, checked)
  override def toW(checked: Boolean) = Cell.toW(edges.head, checked)
  override def toS(checked: Boolean) = Cell.toS(edges.last, checked)
  def allToN(checked: Boolean) = MultiUnitCell(edges.map(e => Cell.toN(e, checked)))
  def allToE(checked: Boolean) = MultiUnitCell(edges.map(e => Cell.toE(e, checked)))
  def allToW(checked: Boolean) = MultiUnitCell(edges.map(e => Cell.toW(e, checked)))
  def allToS(checked: Boolean) = MultiUnitCell(edges.map(e => Cell.toS(e, checked)))
}
sealed trait SingleUnitCell[R, C] extends EdgeCell[R, C]
final case class CrossEdgeCell[R, C](h: HStraightEdgeCell[R, C], v: VStraightEdgeCell[R, C]) extends SingleUnitCell[R, C] {
  override def toN(checked: Boolean) = v.toN(checked)
  override def toE(checked: Boolean) = h.toE(checked)
  override def toW(checked: Boolean) = h.toW(checked)
  override def toS(checked: Boolean) = v.toS(checked)
}
sealed trait SingleEdgeCell[R, C] extends SingleUnitCell[R, C] {
  val edge: Edge[R, C]
}
sealed trait ArrowEdgeCell[R, C] extends SingleEdgeCell[R, C] {
  val basic: SingleEdgeCell[R, C]
  val edge = basic.edge
}

sealed trait CornerEdgeCell[R, C] extends SingleEdgeCell[R, C]
sealed trait CornerLineCell[R, C] extends CornerEdgeCell[R, C]
sealed trait ECornerLineCell[R, C] extends CornerLineCell[R, C]
sealed trait SCornerLineCell[R, C] extends CornerLineCell[R, C]
final case class NECornerCell[R, C](edge: Edge[R, C]) extends CornerLineCell[R, C] with ECornerLineCell[R, C] {
  override def toN(checked: Boolean) = Some(VStraightCell(edge))
  override def toE(checked: Boolean) = Some(HStraightCell(edge))
}
final case class NWCornerCell[R, C](edge: Edge[R, C]) extends CornerLineCell[R, C] {
  override def toN(checked: Boolean) = Some(VStraightCell(edge))
  override def toW(checked: Boolean) = Some(HStraightCell(edge))
}
final case class SECornerCell[R, C](edge: Edge[R, C]) extends CornerLineCell[R, C] with SCornerLineCell[R, C] with ECornerLineCell[R, C] {
  override def toS(checked: Boolean) = Some(VStraightCell(edge))
  override def toE(checked: Boolean) = Some(HStraightCell(edge))
}
final case class SWCornerCell[R, C](edge: Edge[R, C]) extends CornerLineCell[R, C] with SCornerLineCell[R, C] {
  override def toS(checked: Boolean) = Some(VStraightCell(edge))
  override def toW(checked: Boolean) = Some(HStraightCell(edge))
}
sealed trait CornerArrowCell[R, C] extends CornerEdgeCell[R, C] with ArrowEdgeCell[R, C] {
  val basic: CornerLineCell[R, C]
}
final case class ECornerArrowCell[R, C](basic: ECornerLineCell[R, C]) extends CornerArrowCell[R, C] {
  override def toN(checked: Boolean) = basic.toN(checked)
  override def toE(checked: Boolean) = if (checked) throw new IllegalStateException else basic.toE(checked)
  override def toS(checked: Boolean) = basic.toS(checked)
}
final case class SCornerArrowCell[R, C](basic: SCornerLineCell[R, C]) extends CornerArrowCell[R, C] {
  override def toE(checked: Boolean) = basic.toE(checked)
  override def toW(checked: Boolean) = basic.toW(checked)
  override def toS(checked: Boolean) = if (checked) throw new IllegalStateException else basic.toS(checked)
}

sealed trait StraightEdgeCell[R, C] extends SingleEdgeCell[R, C]
sealed trait HStraightEdgeCell[R, C] extends StraightEdgeCell[R, C]
sealed trait VStraightEdgeCell[R, C] extends StraightEdgeCell[R, C]
sealed trait StraightLineCell[R, C] extends StraightEdgeCell[R, C]
final case class HStraightCell[R, C](edge: Edge[R, C]) extends StraightLineCell[R, C] with HStraightEdgeCell[R, C] {
  override def toE(checked: Boolean) = Some(this)
  override def toW(checked: Boolean) = Some(this)
}
final case class VStraightCell[R, C](edge: Edge[R, C]) extends StraightLineCell[R, C] with VStraightEdgeCell[R, C] {
  override def toN(checked: Boolean) = Some(this)
  override def toS(checked: Boolean) = Some(this)
}
sealed trait StraightArrowCell[R, C] extends StraightEdgeCell[R, C] with ArrowEdgeCell[R, C] {
  val basic: StraightLineCell[R, C]
}
final case class EStraightArrowCell[R, C](basic: HStraightCell[R, C]) extends StraightArrowCell[R, C] with HStraightEdgeCell[R, C] {
  override def toE(checked: Boolean) = if (checked) throw new IllegalStateException else basic.toE(checked)
  override def toW(checked: Boolean) = basic.toW(checked)
}
final case class SStraightArrowCell[R, C](basic: VStraightCell[R, C]) extends StraightArrowCell[R, C] with VStraightEdgeCell[R, C] {
  override def toN(checked: Boolean) = basic.toN(checked)
  override def toS(checked: Boolean) = if (checked) throw new IllegalStateException else basic.toS(checked)
}