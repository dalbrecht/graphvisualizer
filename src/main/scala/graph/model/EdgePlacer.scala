/* Copyright 2014 Dennis Albrecht
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package graph
package model

import Cell.toE
import Cell.toS
import content.Edge

private[graph] class EdgePlacer[R, C](grid: Grid[R, C], update: (((Option[R], Int), (Option[C], Int)), Cell[R, C]) => Unit) {
  type ID = ((Option[R], Int), (Option[C], Int))
  private def place(id: ID, h: HStraightEdgeCell[R, C]): Unit = {
    if (id._1._2 == 0)
      throw new IllegalArgumentException
    if (id._2._2 == 0) {
      update(id, grid.getCell(id._1._1, id._1._2, id._2._1, id._2._2) match {
        case EmptyCell() => h
        case MultiUnitCell(edges) => MultiUnitCell(edges.map(_ match {
          case None => Some(h)
          case Some(e) => Some(e match {
            case v: VStraightEdgeCell[R, C] => CrossEdgeCell(h, v)
            case _ => throw new IllegalStateException
          })
        }))
        case _ => throw new IllegalStateException
      })
    } else {
      update(id, grid.getCell(id._1._1, id._1._2, id._2._1, id._2._2) match {
        case EmptyCell() => h
        case v: VStraightEdgeCell[R, C] => CrossEdgeCell(h, v)
        case _ => throw new IllegalStateException
      })
    }
  }
  private def place(id: ID, v: VStraightEdgeCell[R, C]): Unit = {
    if (id._2._2 == 0)
      throw new IllegalArgumentException
    if (id._1._2 == 0) {
      update(id, grid.getCell(id._1._1, id._1._2, id._2._1, id._2._2) match {
        case EmptyCell() => v
        case MultiUnitCell(edges) => MultiUnitCell(edges.map(_ match {
          case None => Some(v)
          case Some(e) => Some(e match {
            case h: HStraightEdgeCell[R, C] => CrossEdgeCell(h, v)
            case _ => throw new IllegalStateException
          })
        }))
        case _ => throw new IllegalStateException
      })
    } else {
      update(id, grid.getCell(id._1._1, id._1._2, id._2._1, id._2._2) match {
        case EmptyCell() => v
        case h: HStraightEdgeCell[R, C] => CrossEdgeCell(h, v)
        case _ => throw new IllegalStateException
      })
    }
  }
  private def place(id: ID, c: CornerEdgeCell[R, C]): Unit = {
    if (id._1._2 == 0 || id._2._2 == 0) {
      throw new IllegalArgumentException
    } else {
      update(id, grid.getCell(id._1._1, id._1._2, id._2._1, id._2._2) match {
        case EmptyCell() => c
        case _ => throw new IllegalStateException
      })
    }
  }
  private def appendH(id: ID, hO: Option[HStraightEdgeCell[R, C]]): Unit = {
    if (id._1._2 != 0 || id._2._2 == 0)
      throw new IllegalArgumentException
    update(id, grid.getCell(id._1._1, id._1._2, id._2._1, id._2._2) match {
      case EmptyCell() => MultiUnitCell(Seq(hO))
      case v: VStraightEdgeCell[R, C] => MultiUnitCell(Seq(Some(hO.map(CrossEdgeCell(_, v)).getOrElse(v))))
      case MultiUnitCell(edges) =>
        MultiUnitCell(edges :+ ((hO, toS(edges.last, true)) match {
          case (None, None) => None
          case (Some(h), Some(v)) => Some(CrossEdgeCell(h, v))
          case (h, None) => h
          case (None, v) => v
        }))
      case _ => throw new IllegalStateException
    })
  }
  private def appendV(id: ID, vO: Option[VStraightEdgeCell[R, C]]): Unit = {
    if (id._1._2 == 0 || id._2._2 != 0)
      throw new IllegalArgumentException
    update(id, grid.getCell(id._1._1, id._1._2, id._2._1, id._2._2) match {
      case EmptyCell() => MultiUnitCell(Seq(vO))
      case h: HStraightEdgeCell[R, C] => MultiUnitCell(Seq(Some(vO.map(CrossEdgeCell(h, _)).getOrElse(h))))
      case MultiUnitCell(edges) =>
        MultiUnitCell(edges :+ ((toE(edges.last, true), vO) match {
          case (None, None) => None
          case (Some(h), Some(v)) => Some(CrossEdgeCell(h, v))
          case (h, None) => h
          case (None, v) => v
        }))
      case _ => throw new IllegalStateException
    })
  }
  private def append(id: ID, c: CornerEdgeCell[R, C]): Unit = {
    if (id._1._2 == 0 && id._2._2 == 0 || id._1._2 != 0 && id._2._2 != 0)
      throw new IllegalArgumentException
    update(id, grid.getCell(id._1._1, id._1._2, id._2._1, id._2._2) match {
      case EmptyCell() => MultiUnitCell(Seq(Some(c)))
      case MultiUnitCell(edges) =>
        if (id._1._2 == 0) {
          if (toS(edges.last, true).isDefined)
            throw new IllegalStateException
          val nX = c.toN(true)
          MultiUnitCell((if (nX.isDefined) {
            edges.map(_ match {
              case None => Some(nX.get)
              case Some(e) => e match {
                case h: HStraightEdgeCell[R, C] => Some(CrossEdgeCell(h, nX.get))
                case _ => throw new IllegalStateException
              }
            })
          } else edges) :+ Some(c))
        } else {
          if (toE(edges.last, true).isDefined)
            throw new IllegalStateException
          val wX = c.toW(true)
          MultiUnitCell((if (wX.isDefined) {
            edges.map(_ match {
              case None => Some(wX.get)
              case Some(e) => e match {
                case v: VStraightEdgeCell[R, C] => Some(CrossEdgeCell(wX.get, v))
                case _ => throw new IllegalStateException
              }
            })
          } else edges) :+ Some(c))
        }
      case _ => throw new IllegalStateException
    })
  }
  private def eArrow(corner: CornerLineCell[R, C]): CornerArrowCell[R, C] = {
    corner match {
      case c: NECornerCell[R, C] => new ECornerArrowCell(c)
      case c: SECornerCell[R, C] => new ECornerArrowCell(c)
      case _: NWCornerCell[R, C] => throw new IllegalStateException
      case _: SWCornerCell[R, C] => throw new IllegalStateException
    }
  }
  private def sArrow(corner: CornerLineCell[R, C]): CornerArrowCell[R, C] = {
    corner match {
      case c: SECornerCell[R, C] => new SCornerArrowCell(c)
      case c: SWCornerCell[R, C] => new SCornerArrowCell(c)
      case _: NECornerCell[R, C] => throw new IllegalStateException
      case _: NWCornerCell[R, C] => throw new IllegalStateException
    }
  }
  private def resolveCorner(f: ID, c: ID, t: ID, edge: Edge[R, C], rows: List[(Option[R], Int)], cols: List[(Option[C], Int)]): CornerLineCell[R, C] = {
    val SouthEast = if (f._1 == c._1) {
      (rows.indexOf(c._1) < rows.indexOf(t._1), cols.indexOf(c._2) < cols.indexOf(f._2))
    } else {
      (rows.indexOf(c._1) < rows.indexOf(f._1), cols.indexOf(c._2) < cols.indexOf(t._2))
    }
    SouthEast match {
      case (false, true) => NECornerCell(edge)
      case (false, false) => NWCornerCell(edge)
      case (true, true) => SECornerCell(edge)
      case (true, false) => SWCornerCell(edge)
    }
  }
  def placeEdge(edge: Edge[R, C], rows: List[(Option[R], Int)], cols: List[(Option[C], Int)], subRows: Option[R] => Int, subCols: Option[C] => Int): Unit = {
    val path = edge.path
    if (path.length < 2 || path.length == 3) {
      throw new IllegalArgumentException
    } else if (path.length == 2) {
      if (path(0)._1 == path(1)._1) { // if horizontal
        if (subCols(path(0)._2._1) != 0) { // are there any columns in between
          val h :: t = cols.dropWhile(_ != path(0)._2).tail.takeWhile(_ != path(1)._2).reverse
          appendH((path(0)._1, h), Some(EStraightArrowCell(HStraightCell(edge))))
          t.foreach(c => appendH((path(0)._1, c), Some(HStraightCell(edge))))
        }
      } else {
        if (subRows(path(0)._1._1) != 0) {
          val h :: t = rows.dropWhile(_ != path(0)._1).tail.takeWhile(_ != path(1)._1).reverse
          appendV((h, path(0)._2), Some(SStraightArrowCell(VStraightCell(edge))))
          t.foreach(r => appendV((r, path(0)._2), Some(VStraightCell(edge))))
        }
      }
    } else {
      for (i <- 2 to path.length - 3) {
        place(path(i), resolveCorner(path(i - 1), path(i), path(i + 1), edge, rows, cols))
      }
      for (i <- 1 to path.length - 3) {
        if (path(i)._1 == path(i + 1)._1) {
          for (c <- cols.dropWhile(c => c != path(i)._2 && c != path(i + 1)._2).tail.takeWhile(c => c != path(i)._2 && c != path(i + 1)._2)) {
            place((path(i)._1, c), HStraightCell(edge))
          }
        } else {
          for (r <- rows.dropWhile(r => r != path(i)._1 && r != path(i + 1)._1).tail.takeWhile(r => r != path(i)._1 && r != path(i + 1)._1)) {
            place((r, path(i)._2), VStraightCell(edge))
          }
        }
      }
      if (path(0)._1 == path(1)._1) {
        val conn = cols.dropWhile(_._1 != path(0)._2._1).tail.takeWhile(_._1 == path(0)._2._1)
        val (addLine, addNone) = conn.splitAt(conn.indexOf(path(1)._2) + 1)
        val h :: t = addLine.reverse
        append((path(0)._1, h), resolveCorner(path(0), path(1), path(2), edge, rows, cols))
        t.foreach(c => appendH((path(0)._1, c), Some(HStraightCell(edge))))
        addNone.foreach(n => appendH((path(0)._1, n), None))
      } else {
        val conn = rows.dropWhile(_._1 != path(0)._1._1).tail.takeWhile(_._1 == path(0)._1._1)
        val (addLine, addNone) = conn.splitAt(conn.indexOf(path(1)._1) + 1)
        val h :: t = addLine.reverse
        append((h, path(0)._2), resolveCorner(path(0), path(1), path(2), edge, rows, cols))
        t.foreach(r => appendV((r, path(0)._2), Some(VStraightCell(edge))))
        addNone.foreach(n => appendV((n, path(0)._2), None))
      }
      val end = path.length - 2
      if (path(end)._1 == path(end + 1)._1) {
        val conn = cols.dropWhile(_._1 != path(end)._2._1).tail.takeWhile(_._1 == path(end)._2._1)
        val (addNone, addLine) = conn.splitAt(conn.indexOf(path(end)._2))
        val (c, a, ca, l) = if (addLine.length == 1)
          (None, None, addLine.headOption, Seq())
        else
          (addLine.headOption, addLine.lastOption, None, addLine.tail.reverse.tail)
        ca.foreach(c => append((path(end)._1, c), eArrow(resolveCorner(path(end - 1), path(end), path(end + 1), edge, rows, cols))))
        c.foreach(c => append((path(end)._1, c), resolveCorner(path(end - 1), path(end), path(end + 1), edge, rows, cols)))
        a.foreach(a => appendH((path(end)._1, a), Some(EStraightArrowCell(HStraightCell(edge)))))
        l.foreach(c => appendH((path(end)._1, c), Some(HStraightCell(edge))))
        addNone.foreach(n => appendH((path(end)._1, n), None))
      } else {
        val conn = rows.dropWhile(_._1 != path(end)._1._1).tail.takeWhile(_._1 == path(end)._1._1)
        val (addNone, addLine) = conn.splitAt(conn.indexOf(path(end)._1))
        val (c, a, ca, l) = if (addLine.length == 1)
          (None, None, addLine.headOption, Seq())
        else
          (addLine.headOption, addLine.lastOption, None, addLine.tail.reverse.tail)
        ca.foreach(r => append((r, path(end)._2), sArrow(resolveCorner(path(end - 1), path(end), path(end + 1), edge, rows, cols))))
        c.foreach(r => append((r, path(end)._2), resolveCorner(path(end - 1), path(end), path(end + 1), edge, rows, cols)))
        a.foreach(a => appendV((a, path(end)._2), Some(SStraightArrowCell(VStraightCell(edge)))))
        l.foreach(r => appendV((r, path(end)._2), Some(VStraightCell(edge))))
        addNone.foreach(n => appendV((n, path(end)._2), None))
      }
    }
  }
}