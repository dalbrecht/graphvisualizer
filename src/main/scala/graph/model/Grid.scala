/* Copyright 2014 Dennis Albrecht
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package graph
package model

import scala.collection.mutable.HashMap

import content.Edge
import content.Node

class Grid[R, C] extends GridChangeObservable[R, C] {
  val empty: EmptyCell[R, C] = new EmptyCell
  private val _placer = new EdgePlacer(this, update)
  private var _rows: List[(Option[R], Int)] = List((None, 0))
  private var _cols: List[(Option[C], Int)] = List((None, 0))
  private val _subRows: HashMap[Option[R], Int] = new HashMap[Option[R], Int] {
    put(None, 0)
  }
  private val _subCols: HashMap[Option[C], Int] = new HashMap[Option[C], Int] {
    put(None, 0)
  }
  /* Constraints on Cells that cannot be guaranteed by the type-system:
   * - EmptyCells aren't placed on this grid explicitly but implicitly on every empty coordinate
   * - NodeCells are placed only on coordinates ((_, 0), (_, 0)) and these coordinates are occupied by NodeCells only
   * - MultiUnitCells are placed only on coordinates ((_, !0), (_, 0)) or ((_, 0), (_, !0))
   * - the coordinates ((_, !0), (_, 0)) and ((_, 0), (_, !0)) are occupied by MultiUnitCells and StraithEdges* only
   *   * = but only if aligned orthogonal to the line between the neighboring nodes
   * - the elements of a MultiUnitCells are aligned orthogonal to the line between the neighboring nodes starting with the topmost or leftmost element
   */
  private val _cells: HashMap[((Option[R], Int), (Option[C], Int)), Cell[R, C]] = new HashMap
  private val _cacheR: HashMap[(Option[R], Int), GridRow[R, C]] = new HashMap
  private val _cacheC: HashMap[(Option[C], Int), GridColumn[R, C]] = new HashMap
  private val _cacheX: HashMap[(Option[R], Int, Option[C], Int), GridCell[R, C]] = new HashMap

  def getRow(r: Option[R], sub: Int): GridRow[R, C] = {
    if (_rows.contains((r, sub))) {
      _cacheR.synchronized {
        if (_cacheR.contains((r, sub)))
          _cacheR((r, sub))
        else {
          val row = new GridRow(this, r, sub)
          _cacheR put ((r, sub), row)
          row
        }
      }
    } else {
      throw new IllegalArgumentException("Given row is not part of this Grid")
    }
  }
  def getColumn(c: Option[C], sub: Int): GridColumn[R, C] = {
    if (_cols.contains((c, sub))) {
      _cacheC.synchronized {
        if (_cacheC.contains((c, sub)))
          _cacheC((c, sub))
        else {
          val col = new GridColumn(this, c, sub)
          _cacheC put ((c, sub), col)
          col
        }
      }
    } else {
      throw new IllegalArgumentException("Given column is not part of this Grid")
    }
  }
  def getGridCell(r: Option[R], subR: Int, c: Option[C], subC: Int): GridCell[R, C] = {
    if (_rows.contains((r, subR)) && _cols.contains((c, subC))) {
      _cacheX.synchronized {
        if (_cacheX.contains((r, subR, c, subC)))
          _cacheX((r, subR, c, subC))
        else {
          val cell = new GridCell(this, r, subR, c, subC)
          _cacheX put ((r, subR, c, subC), cell)
          cell
        }
      }
    } else
      throw new IllegalArgumentException("Address of cell is invalid")
  }
  def getCell(r: Option[R], subR: Int, c: Option[C], subC: Int): Cell[R, C] = {
    if (_rows.contains((r, subR)) && _cols.contains((c, subC)))
      _cells.getOrElse(((r, subR), (c, subC)), empty)
    else
      throw new IllegalArgumentException("Address of cell is invalid")
  }
  def rows(): List[(Option[R], Int)] = _rows.tail.to[List]
  def columns(): List[(Option[C], Int)] = _cols.tail.to[List]
  def getRowCount(): Int = _rows.tail.length
  def getColumnCount(): Int = _cols.tail.length

  private[graph] def setNode(r: R, c: C, node: Node[R, C]): Unit = {
    update(((Some(r), 0), (Some(c), 0)), NodeCell(node))
  }
  private[graph] def addEdge(edge: Edge[R, C]): Unit = {
    _placer.placeEdge(edge, _rows, _cols, _subRows.apply, _subCols.apply)
  }
  private[graph] def addRow(r: R): Unit = {
    val _r = Some(r)
    _rows = _rows :+ (_r, 0)
    _subRows put (_r, 0)
    fire(RowAddition(_rows.tail.length - 1, _r, 0))
  }
  private[graph] def addCol(c: C): Unit = {
    val _c = Some(c)
    _cols = _cols :+ (_c, 0)
    _subCols put (_c, 0)
    fire(ColumnAddition(_cols.tail.length - 1, _c, 0))
  }
  private[graph] def newSubRow(r: Option[R]): (Option[R], Int) = {
    val next = _subRows(r) + 1
    _subRows put (r, next)
    insertRow(r, next)
    (r, next)
  }
  private[graph] def newSubCol(c: Option[C]): (Option[C], Int) = {
    val next = _subCols(c) + 1
    _subCols put (c, next)
    insertCol(c, next)
    (c, next)
  }
  private def insertRow(r: Option[R], sub: Int): Unit = {
    val index = _rows.indexOf((r, 0))
    val (h, t) = _rows.splitAt(index + 1)
    _rows = h ++ ((r, sub) :: t)
    if (!t.isEmpty)
      copyRow(t.head, (r, sub))
    fire(RowAddition(index, r, sub))
  }
  private def insertCol(c: Option[C], sub: Int): Unit = {
    val index = _cols.indexOf((c, 0))
    val (h, t) = _cols.splitAt(index + 1)
    _cols = h ++ ((c, sub) :: t)
    if (!t.isEmpty)
      copyCol(t.head, (c, sub))
    fire(ColumnAddition(index, c, sub))
  }
  private def copyRow(src: (Option[R], Int), trg: (Option[R], Int)): Unit = {
    for (c <- _cols) {
      (getCell(src._1, src._2, c._1, c._2) match {
        case EmptyCell() => None
        case NodeCell(n) => {
          val edges = n.edges.filter(e => e.to == n && e.path.length == 2 && e.path(0)._2 == e.path(1)._2)
          if (edges.isEmpty)
            None
          else
            Some(MultiUnitCell(edges.toSeq.map(e => Some(SStraightArrowCell(VStraightCell(e))))))
        }
        case multi: MultiUnitCell[R, C] =>
          if (c._2 == 0) Some(multi.allToN(true)) else multi.toN(true)
        case cell: SingleUnitCell[R, C] => cell.toN(true)
      }).foreach(cell => update((trg, c), cell))
    }
  }
  private def copyCol(src: (Option[C], Int), trg: (Option[C], Int)): Unit = {
    for (r <- _rows) {
      (getCell(r._1, r._2, src._1, src._2) match {
        case EmptyCell() => None
        case NodeCell(n) => {
          val edges = n.edges.filter(e => e.to == n && e.path.length == 2 && e.path(0)._1 == e.path(1)._1)
          if (edges.isEmpty)
            None
          else
            Some(MultiUnitCell(edges.toSeq.map(e => Some(EStraightArrowCell(HStraightCell(e))))))
        }
        case multi: MultiUnitCell[R, C] =>
          if (r._2 == 0) Some(multi.allToW(true)) else multi.toW(true)
        case cell: SingleUnitCell[R, C] => cell.toW(true)
      }).foreach(cell => update((r, trg), cell))
    }
  }
  private def update(index: ((Option[R], Int), (Option[C], Int)), cell: Cell[R, C]): Unit = {
    _cells put (index, cell)
    fire(CellUpdate(index._1._1, index._1._2, index._2._1, index._2._2, cell))
  }
  override protected def fire(change: GridChange[R, C]): Unit = {
    super.fire(change)
    change match {
      case u: CellUpdate[R, C] => {
        _cacheR.get((u.r, u.subR)).foreach(_.update(this, u))
        _cacheC.get((u.c, u.subC)).foreach(_.update(this, u))
        _cacheX.get((u.r, u.subR, u.c, u.subC)).foreach(_.update(this, u))
      }
      case r: RowAddition[R, C] => {
        _cacheR.get((r.r, r.sub)).foreach(_.update(this, r))
        _cacheC.foreach(_._2.update(this, r))
      }
      case c: ColumnAddition[R, C] => {
        _cacheC.get((c.c, c.sub)).foreach(_.update(this, c))
        _cacheR.foreach(_._2.update(this, c))
      }
    }
  }
}