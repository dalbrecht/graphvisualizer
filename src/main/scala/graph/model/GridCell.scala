/* Copyright 2014 Dennis Albrecht
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package graph
package model

class GridCell[R, C] private[model] (val grid: Grid[R, C], val row: Option[R], val subRow: Int, val column: Option[C], val subCol: Int) extends GridChangeRelay[R, C] {
  def getValue(): Cell[R, C] = grid.getCell(row, subRow, column, subCol)
}