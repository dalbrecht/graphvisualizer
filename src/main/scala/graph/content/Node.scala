/* Copyright 2014 Dennis Albrecht
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package graph
package content

import java.{ util => ju }
import java.util.ArrayList
import java.util.HashSet

import scala.collection.JavaConversions.asScalaBuffer
import scala.collection.JavaConversions.seqAsJavaList
import scala.collection.JavaConversions.setAsJavaSet
import scala.language.existentials

import style.NodeStyle

final class Node[Row, Col] private[content] (val title: String, val style: NodeStyle, val info: Seq[Information] = Nil) {
  private var _graph: Option[Graph[Row, Col]] = None
  private var _row: Option[Row] = None
  private var _col: Option[Col] = None
  private var _edges: Set[Edge[Row, Col]] = Set()
  private var _subgraph: Option[Graph[_, _]] = None

  def getInfo(): ju.List[Information] = new ArrayList(info)
  def edges(): Set[Edge[Row, Col]] = _edges
  def getEdges(): ju.Set[Edge[Row, Col]] = new HashSet(_edges)
  def isPartOfGraph(): Boolean = _graph.isDefined
  def getContainingGraph(): Graph[Row, Col] = {
    _graph.synchronized {
      if (isPartOfGraph)
        _graph.get
      else
        throw new IllegalStateException("This node isn't part of some graph!")
    }
  }
  def getRow(): Row = {
    _row.synchronized {
      if (isPartOfGraph)
        _row.get
      else
        throw new IllegalStateException("This node isn't part of some graph and hence row isn't set!")
    }
  }
  def getColumn(): Col = {
    _col.synchronized {
      if (isPartOfGraph)
        _col.get
      else
        throw new IllegalStateException("This node isn't part of some graph and hence column isn't set!")
    }
  }
  def getSubgraph(): Graph[_, _] = _subgraph.getOrElse(null)

  private[graph] def setPosition(graph: Option[Graph[Row, Col]] = None, row: Option[Row] = None, col: Option[Col] = None): Unit = {
    this.synchronized {
      if (graph.isEmpty) {
        if (_graph.isDefined) {
          _graph = None
          _row = None
          _col = None
        }
      } else if (row.isDefined && col.isDefined) {
        _graph = graph
        _row = row
        _col = col
      } else {
        throw new IllegalArgumentException("Position-coordinates cannot be empty if graph is valid!")
      }
    }
  }
  private[graph] def addEdge(edge: Edge[Row, Col]): Unit = {
    _edges.synchronized { _edges = _edges + edge }
  }
  private[graph] def removeEdge(edge: Edge[Row, Col]): Unit = {
    _edges.synchronized { _edges = _edges - edge }
  }
  private[graph] def clearEdges(): Unit = {
    _edges.synchronized { _edges = Set() }
  }
  private[graph] def setSubgraph(subgraph: Graph[_, _]): Unit = {
    _subgraph.synchronized { _subgraph = if (subgraph == null) None else Some(subgraph) }
  }
}

object NodeFactory {
  def apply[Row, Col](title: String, style: NodeStyle): Node[Row, Col] = {
    new Node(title, style)
  }
  def apply[Row, Col](title: String, style: NodeStyle, info: Seq[Information]): Node[Row, Col] = {
    new Node(title, style, info.to[Seq])
  }
  def apply[Row, Col](title: String, style: NodeStyle, info: ju.List[Information]): Node[Row, Col] = {
    new Node(title, style, info.to[Seq])
  }
}