/* Copyright 2014 Dennis Albrecht
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package graph
package content

import style.EdgeStyle

final class Edge[Row, Col] private[content] (private[graph] var from: Node[Row, Col], private[graph] var to: Node[Row, Col], val style: EdgeStyle,
  private[graph] val path: Seq[((Option[Row], Int), (Option[Col], Int))], private[graph] val subRow: Option[(Option[Row], Int)], private[graph] val subCol: Option[(Option[Col], Int)]) {
  def getFrom(): Node[Row, Col] = from
  def getTo(): Node[Row, Col] = to
}

object Edge {
  private[graph] def apply[Row, Col](from: Node[Row, Col], to: Node[Row, Col], style: EdgeStyle,
    path: Seq[((Option[Row], Int), (Option[Col], Int))], subRow: Option[(Option[Row], Int)], subCol: Option[(Option[Col], Int)]): Edge[Row, Col] = {
    new Edge(from, to, style, path, subRow, subCol)
  }
}