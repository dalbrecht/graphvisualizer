/* Copyright 2014 Dennis Albrecht
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package graph
package content

import java.{ util => ju }
import java.util.ArrayList

import scala.collection.JavaConversions.asScalaBuffer
import scala.collection.JavaConversions.seqAsJavaList

final class Information private[graph] (val title: String, val elems: Seq[String] = Nil) {
  def getElems(): ju.List[String] = new ArrayList(elems)
  def add(elem: String): Information = new Information(title, elems :+ elem)
}

object Information {
  def apply(title: String): Information = {
    new Information(title)
  }
  def apply(title: String, elems: Seq[String]): Information = {
    new Information(title, elems.to[Seq])
  }
  def apply(title: String, elems: java.util.List[String]): Information = {
    new Information(title, elems.to[Seq])
  }
}