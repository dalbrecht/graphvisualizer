/* Copyright 2014 Dennis Albrecht
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package util

/**
 * Wrapper holding two maps to synthesize a bidirectional map.
 */
class BiDiMap[K, V] {
  private var k2v: Map[K, V] = Map()
  private var v2k: Map[V, K] = Map()
  def put(k: K, v: V) = {
    if (k2v.isDefinedAt(k)) {
      val v = k2v(k)
      k2v -= k
      v2k -= v
    }
    if (v2k.isDefinedAt(v)) {
      val k = v2k(v)
      k2v -= k
      v2k -= v
    }
    k2v += k -> v
    v2k += v -> k
  }
  def key2value: Map[K, V] = k2v
  def value2key: Map[V, K] = v2k
  def key(k: K): Boolean = k2v.isDefinedAt(k)
  def value(v: V): Boolean = v2k.isDefinedAt(v)
}