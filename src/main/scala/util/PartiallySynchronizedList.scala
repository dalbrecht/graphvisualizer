/* Copyright 2014 Dennis Albrecht
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package util

/**
 * mutual, incomplete (may be extended if needed) List that synchronizes some accesses
 */
class PartiallySynchronizedList[T] {
  private var internalList: List[T] = Nil
  def add(el: T): Unit = {
    add(el, () => ())
  }
  def add(el: T, success: () => Unit): Unit = {
    if (!contains(el))
      this.synchronized {
        if (!contains(el)) {
          internalList ::= el
          success()
        }
      }
  }
  def contains(el: T): Boolean = internalList.contains(el)
  def get(index: Int): T = {
    internalList.synchronized {
      internalList(internalList.length - 1 - index)
    }
  }
  def indexOf(el: T): Int = {
    internalList.synchronized {
      internalList.length - 1 - internalList.indexOf(el)
    }
  }
  def lastOption(): Option[T] = internalList.headOption
  def toList(): List[T] = internalList.reverse
}