/* Copyright 2014 Dennis Albrecht
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package util

/**
 * mutual, incomplete (may be extended if needed) List that synchronizes some accesses
 */
class PartiallySynchronizedMap[T1, T2] {
  private var internalMap: Map[T1, T2] = Map()
  def containsKey(key: T1): Boolean = internalMap.isDefinedAt(key)
  def get(key: T1): T2 = internalMap(key)
  def getOrDefault(key: T1, default: T2): T2 = {
    this.synchronized {
      if (internalMap.isDefinedAt(key)) {
        internalMap(key)
      } else {
        internalMap += key -> default
        default
      }
    }
  }
  def put(key: T1, value: T2): Option[T2] = {
    this.synchronized {
      val old = internalMap.get(key)
      internalMap += key -> value
      old
    }
  }
  def remove(key: T1): Option[T2] = {
    this.synchronized {
      val value = internalMap.get(key)
      internalMap -= key
      value
    }
  }
}