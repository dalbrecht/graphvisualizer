/* Copyright 2014 Dennis Albrecht
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package examples

import java.awt.Color

import graph.Graph
import graph.content.Information
import graph.content.NodeFactory
import graph.style.EdgeStyle.large
import graph.style.EdgeStyle.medium
import graph.style.EdgeStyle.mediumDashed
import graph.style.EdgeStyle.small
import graph.style.EdgeStyle.smallDashed
import graph.style.NodeStyle.default
import ui.javafx.JavaFxUI

object ScalaExample extends App {
  val root = new Graph[String, Int]("Test")
  JavaFxUI.visualize(root)
  actions.foreach(a => {
    Thread.sleep(500)
    val start = System.currentTimeMillis()
    a(root)
    //    println(System.currentTimeMillis() - start)
  })

  def actions: List[root.type => Unit] = {
    List(
      _.set(NodeFactory("HeyHo", default, Seq(Information("Foo", Seq("Bar")))), "1", 1),
      _.set(NodeFactory("HoHey", default), "2", 2),
      _.set(NodeFactory("FPos", default), "1", 2),
      _.set(NodeFactory("FNeg", default), "2", 1),
      _.addEdge("1", 1, "2", 1, medium(Color.GREEN)),
      _.addEdge("1", 1, "1", 2, small(Color.BLUE)),
      _.set(NodeFactory("Right", default), "1", 3),
      _.addEdge("1", 1, "1", 3, mediumDashed),
      _.addEdge("1", 1, "1", 3, smallDashed),
      _.set(NodeFactory("Bottom", default), "3", 1),
      _.addEdge("1", 1, "3", 1, medium(Color.CYAN)),
      _.addEdge("1", 1, "3", 1, medium(Color.MAGENTA)),
      _.set(NodeFactory("End", default), "3", 3),
      _.set(NodeFactory("Exit", default), "4", 4),
      _.addEdge("1", 1, "2", 1, medium(Color.ORANGE)),
      _.addEdge("1", 1, "1", 2, large(Color.RED)),
      _.addEdge("4", 4, "2", 2, small(Color.ORANGE)),
      _.addEdge("4", 4, "3", 1, small(Color.BLUE)),
      _.addEdge("4", 4, "1", 3, small(Color.GREEN)),
      g => pollute(g.addSubGraph(g.get("2", 2), "Inner")),
      g => println(s"${g.name} is finished"))
  }
  private def pollute(g: Graph[Int, String]): Unit = {
    g.set(NodeFactory("Home", default), 1, "1") // perhaps update
    g.set(NodeFactory("Sweet", default), 1, "2")
    g.set(NodeFactory("Home", default), 1, "3")
    g.set(NodeFactory("Hello", default), 2, "2")
    g.set(NodeFactory("World", default), 2, "3")
    g.set(NodeFactory("Romans", default), 3, "2")
    g.set(NodeFactory("vs", default), 3, "3")
    g.set(NodeFactory("Greeks", default), 3, "4")
    g.set(NodeFactory("That's", default), 4, "3")
    g.set(NodeFactory("All", default), 4, "4")
    g.set(NodeFactory("Folks", default), 4, "5")
  }
}