/* Copyright 2014 Dennis Albrecht
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ui.javafx

import graph.Graph
import graph.model.GridRow
import javafx.scene.{ Node => FxNode }
import javafx.scene.control.TableView
import javafx.stage.Stage
import observable.ObservableGridColumnList
import observable.ObservableGridRowList
import observable.ObservableListProxy

class Layer[R, C](graph: Graph[R, C], navigator: Graph[_, _] => Unit, locator: FxNode => Unit, register: Stage => Unit) {
  val name = graph.name
  private val _table = new TableView[GridRow[R, C]] {
    setStyle("-fx-table-cell-border-color:transparent")
  }
  private val _columns = new ColumnFactory(new ObservableListProxy(_table.getColumns), new ObservableGridColumnList(graph.grid), navigator, register)
  private val _rows = new ObservableGridRowList(graph.grid)
  private var _active = false
  def isActive(): Boolean = _active
  def activate(): Unit = {
    if (!_active) {
      _active = true
      _columns.activate
      _table.setItems(_rows)
      locator(_table)
    }
  }
  def deactivate(): Unit = {
    _active = false
    _columns.deactivate
    _table.setItems(null)
  }
}