/* Copyright 2014 Dennis Albrecht
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ui.javafx.cells

import GraphTableCell.convert
import graph.Graph
import graph.content.Node
import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.scene.control.Label
import javafx.scene.control.TextArea
import javafx.scene.input.MouseButton
import javafx.scene.input.MouseEvent
import javafx.scene.layout.BorderPane
import javafx.stage.Stage
import javafx.stage.StageStyle

class TableNodeCell[R, C](node: Node[R, C], navigator: Graph[_, _] => Unit, register: Stage => Unit) extends Label {
  private var info: Option[Stage] = None
  setText(node.title)
  setTextFill(node.style.color)
  addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler[MouseEvent] {
    def handle(event: MouseEvent): Unit = {
      (event.getButton, event.getClickCount) match {
        case (MouseButton.PRIMARY, 2) => if (node.getSubgraph != null) navigator(node.getSubgraph)
        case (MouseButton.SECONDARY, 1) => info match {
          case None => {
            val secondaryStage = new Stage(StageStyle.UTILITY)
            register(secondaryStage)
            info = Some(secondaryStage)
            secondaryStage.setTitle(node.title.takeWhile(_ != '\n'))
            val pane = new BorderPane
            secondaryStage.setScene(new Scene(pane))
            pane.setCenter(new TextArea(s"${node.title}\n\n${node.info.map(i => s"${i.title}:\n${i.elems.map('\t' + _).mkString("\n")}").mkString("\n")}") {
              setEditable(false)
            })
            secondaryStage.show
          }
          case Some(i) => i.hide; i.show
        }
        case _ =>
      }
    }
  })
}