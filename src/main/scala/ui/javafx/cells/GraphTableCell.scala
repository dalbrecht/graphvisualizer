/* Copyright 2014 Dennis Albrecht
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ui.javafx
package cells

import scala.language.implicitConversions

import graph.Graph
import graph.model.Cell
import graph.model.EmptyCell
import graph.model.GridRow
import graph.model.MultiUnitCell
import graph.model.NodeCell
import graph.model.SingleUnitCell
import javafx.geometry.Pos
import javafx.scene.control.TableCell
import javafx.scene.paint.Color
import javafx.stage.Stage

object GraphTableCell {
  val SingleEdgeWidth = 13
  val SingleEdgeHeight = 13
  implicit def convert(color: java.awt.Color): Color = {
    Color.rgb(color.getRed, color.getGreen, color.getBlue, 1 - color.getTransparency / 255f)
  }
}

class GraphTableCell[R, C](col: (Option[C], Int), navigator: Graph[_, _] => Unit, register: Stage => Unit) extends TableCell[GridRow[R, C], Cell[R, C]] {
  setAlignment(Pos.TOP_LEFT)
  setStyle("-fx-padding:-1px")
  //setStyle("-fx-border-color:transparent;-fx-border-width:0")
  override protected def updateItem(cell: Cell[R, C], empty: Boolean): Unit = {
    if (cell != getItem()) {
      super.updateItem(cell, empty)
      cell match {
        case null | EmptyCell() => super.setGraphic(null)
        case NodeCell(n) => super.setGraphic(new TableNodeCell(n, navigator, register))
        case edge: SingleUnitCell[R, C] => super.setGraphic(TableEdgeCellFactory(edge, super.tableColumnProperty, super.tableRowProperty))
        case edge: MultiUnitCell[R, C] => super.setGraphic(TableEdgeCellFactory(edge, col._2 == 0, super.tableColumnProperty, super.tableRowProperty))
      }
    }
  }
}