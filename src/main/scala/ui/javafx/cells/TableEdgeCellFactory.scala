/* Copyright 2014 Dennis Albrecht
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ui.javafx
package cells

import scala.language.implicitConversions
import scala.math.max

import GraphTableCell.SingleEdgeHeight
import GraphTableCell.SingleEdgeWidth
import GraphTableCell.convert
import graph.model.Cell
import graph.model.CrossEdgeCell
import graph.model.ECornerArrowCell
import graph.model.EStraightArrowCell
import graph.model.GridRow
import graph.model.HStraightCell
import graph.model.MultiUnitCell
import graph.model.NECornerCell
import graph.model.NWCornerCell
import graph.model.SCornerArrowCell
import graph.model.SECornerCell
import graph.model.SStraightArrowCell
import graph.model.SWCornerCell
import graph.model.SingleEdgeCell
import graph.model.SingleUnitCell
import graph.model.VStraightCell
import graph.style.EdgeStyle
import javafx.beans.property.ReadOnlyDoubleProperty
import javafx.beans.property.ReadOnlyObjectProperty
import javafx.beans.value.ChangeListener
import javafx.beans.value.ObservableValue
import javafx.scene.Node
import javafx.scene.canvas.Canvas
import javafx.scene.canvas.GraphicsContext
import javafx.scene.control.TableColumn
import javafx.scene.control.TableRow
import javafx.scene.layout.GridPane
import observable.ObservableCalculation

object TableEdgeCellFactory {
  val epsilon = 0.5
  def apply[R, C](edge: SingleUnitCell[R, C], width: ReadOnlyObjectProperty[TableColumn[GridRow[R, C], Cell[R, C]]], height: ReadOnlyObjectProperty[TableRow[_]]): Node = {
    val pane = new GridPane
    val main = newCanvas(edge)
    pane.add(main, 0, 0)
    pane.setMinSize(main.getWidth, main.getHeight)
    registerWidthListener(edge.toE(false).map(ext => () => newCanvas(ext)), pane, width)
    registerHeightListener(edge.toS(false).map(ext => () => newCanvas(ext)), Some(SingleEdgeHeight), pane, height)
    pane
  }
  def apply[R, C](edge: MultiUnitCell[R, C], horizontal: Boolean, width: ReadOnlyObjectProperty[TableColumn[GridRow[R, C], Cell[R, C]]], height: ReadOnlyObjectProperty[TableRow[_]]): Node = {
    val pane = new GridPane
    val main = newCanvas(edge, horizontal)
    pane.add(main, 0, 0)
    pane.setMinSize(main.getWidth, main.getHeight)
    if (horizontal) {
      registerWidthListener(edge.toE(false).map(ext => () => newCanvas(ext)), pane, width)
      registerHeightListener(Some(() => newCanvas(edge.allToS(false), true)), Some(SingleEdgeHeight), pane, height)
    } else {
      registerWidthListener(Some(() => newCanvas(edge.allToE(false), true)), pane, width)
      registerHeightListener(edge.toS(false).map(ext => () => newCanvas(ext)), Some(SingleEdgeHeight * edge.edges.length), pane, height)
    }
    pane
  }
  private def registerWidthListener[R, C](ext: Option[() => Canvas], pane: GridPane, width: ReadOnlyObjectProperty[TableColumn[GridRow[R, C], Cell[R, C]]]): Unit = {
    if (ext.isDefined) {
      var elems = 1
      def resize(targetWidth: Double): Unit = {
        while (targetWidth - epsilon > elems * SingleEdgeWidth) {
          pane.add(ext.get(), elems, 0)
          elems += 1
        }
      }
      val listener = new ChangeListener[Number] {
        def changed(observable: ObservableValue[_ <: Number], oldVal: Number, newVal: Number): Unit = {
          resize(newVal.asInstanceOf[Double])
        }
      }
      width.addListener(new ChangeListener[TableColumn[GridRow[R, C], Cell[R, C]]] {
        def changed(observable: ObservableValue[_ <: TableColumn[GridRow[R, C], Cell[R, C]]], oldVal: TableColumn[GridRow[R, C], Cell[R, C]], newVal: TableColumn[GridRow[R, C], Cell[R, C]]): Unit = {
          if (width == observable) {
            if (oldVal != null) {
              oldVal.widthProperty.removeListener(listener)
            }
            if (newVal != null) {
              newVal.widthProperty.addListener(listener)
            }
          }
        }
      })
      if (width.getValue != null) {
        width.getValue.widthProperty.addListener(listener)
        if (width.getValue.widthProperty != null)
          resize(width.getValue.widthProperty.getValue)
      }
    }
  }
  private def registerHeightListener[R, C](ext: Option[() => Canvas], min: Option[Double], pane: GridPane, height: ReadOnlyObjectProperty[TableRow[_]]): Unit = {
    if (ext.isDefined) {
      def bind(prop: ObservableValue[Number]): Unit = {
        if (min.isDefined) {
          ObservableCalculation.calc[Number, Number](n => Math.max(n.asInstanceOf[Double], 50), prop)
        } else {
          pane.prefHeightProperty.bind(prop)
        }
      }
      var elems = 1
      def resize(targetHeight: Double): Unit = {
        while (targetHeight - epsilon > elems * SingleEdgeHeight) {
          pane.add(ext.get(), 0, elems)
          elems += 1
        }
      }
      val listener = new ChangeListener[Number] {
        def changed(observable: ObservableValue[_ <: Number], oldVal: Number, newVal: Number): Unit = {
          resize(newVal.asInstanceOf[Double])
        }
      }
      height.addListener(new ChangeListener[TableRow[_]] {
        def changed(observable: ObservableValue[_ <: TableRow[_]], oldVal: TableRow[_], newVal: TableRow[_]): Unit = {
          if (height == observable) {
            if (oldVal != null) {
              oldVal.heightProperty.removeListener(listener)
            }
            if (newVal != null) {
              bind(newVal.heightProperty)
              newVal.heightProperty.addListener(listener)
            }
          }
        }
      })
      if (height.getValue != null) {
        bind(height.getValue.heightProperty)
        height.getValue.heightProperty.addListener(listener)
        if (height.getValue.heightProperty != null)
          resize(height.getValue.heightProperty.getValue)
      }
    }
  }
  private def newCanvas[R, C](edge: SingleUnitCell[R, C]): Canvas = {
    val canvas = new Canvas(SingleEdgeWidth, SingleEdgeHeight)
    draw(edge, canvas.getGraphicsContext2D, 0, 0)
    canvas
  }
  private def newCanvas[R, C](edge: MultiUnitCell[R, C], horizontal: Boolean): Canvas = {
    val canvas = new Canvas(if (horizontal) edge.edges.length * SingleEdgeWidth else SingleEdgeWidth, if (horizontal) SingleEdgeHeight else edge.edges.length * SingleEdgeHeight)
    val gc = canvas.getGraphicsContext2D
    for (i <- 0 until edge.edges.length) {
      edge.edges(i).foreach(draw(_, gc, if (horizontal) i * SingleEdgeWidth else 0, if (horizontal) 0 else i * SingleEdgeHeight))
    }
    canvas
  }
  private def draw[R, C](cell: SingleUnitCell[R, C], g: GraphicsContext, x: Int, y: Int): Unit = {
    def n(style: EdgeStyle): Unit = {
      val h = if (style.dashed) SingleEdgeHeight / 4 else (SingleEdgeHeight + style.size) / 2
      g.fillRect(x + (SingleEdgeWidth - style.size) / 2, y, style.size, h)
      g.fillRect(x + (SingleEdgeWidth - style.size) / 2, y + (SingleEdgeHeight - style.size) / 2, style.size, style.size)
    }
    def e(style: EdgeStyle): Unit = {
      val w = if (style.dashed) max(SingleEdgeWidth / 4, style.size) else (SingleEdgeWidth + style.size + 1) / 2
      g.fillRect(x + (SingleEdgeWidth - style.size) / 2, y + (SingleEdgeHeight - style.size) / 2, w, style.size)
    }
    def s(style: EdgeStyle): Unit = {
      val h = if (style.dashed) max(SingleEdgeHeight / 4, style.size) else (SingleEdgeHeight + style.size + 1) / 2
      g.fillRect(x + (SingleEdgeWidth - style.size) / 2, y + (SingleEdgeHeight - style.size) / 2, style.size, h)
    }
    def w(style: EdgeStyle): Unit = {
      val w = if (style.dashed) SingleEdgeWidth / 4 else (SingleEdgeWidth + style.size) / 2
      g.fillRect(x, y + (SingleEdgeHeight - style.size) / 2, w, style.size)
      g.fillRect(x + (SingleEdgeWidth - style.size) / 2, y + (SingleEdgeHeight - style.size) / 2, style.size, style.size)
    }
    def eArrow(style: EdgeStyle): Unit = {
      val w = (style.size - 1) / 2
      g.clearRect(x + SingleEdgeWidth - w, y + (SingleEdgeHeight - style.size) / 2, w, style.size)
      val mod = style.size % 2
      for (l <- 1 to (w + 4)) {
        val h = 2 * l - mod
        g.fillRect(x + SingleEdgeWidth - l, y + (SingleEdgeHeight - h) / 2, 1, h)
      }
    }
    def sArrow(style: EdgeStyle): Unit = {
      val h = (style.size - 1) / 2
      g.clearRect(x + (SingleEdgeWidth - style.size) / 2, y + SingleEdgeHeight - h, style.size, h)
      val mod = style.size % 2
      for (l <- 1 to (h + 4)) {
        val w = 2 * l - mod
        g.fillRect(x + (SingleEdgeWidth - w) / 2, y + SingleEdgeHeight - l, w, 1)
      }
    }
    cell match {
      case CrossEdgeCell(h, v) => {
        draw(v, g, x, y)
        draw(h, g, x, y)
      }
      case ECornerArrowCell(inner) => {
        draw(inner, g, x, y)
        g.setFill(inner.edge.style.color)
        eArrow(inner.edge.style)
      }
      case EStraightArrowCell(inner) => {
        draw(inner, g, x, y)
        g.setFill(inner.edge.style.color)
        eArrow(inner.edge.style)
      }
      case SCornerArrowCell(inner) => {
        draw(inner, g, x, y)
        g.setFill(inner.edge.style.color)
        sArrow(inner.edge.style)
      }
      case SStraightArrowCell(inner) => {
        draw(inner, g, x, y)
        g.setFill(inner.edge.style.color)
        sArrow(inner.edge.style)
      }
      case NECornerCell(edge) => {
        g.setFill(edge.style.color)
        n(edge.style)
        e(edge.style)
      }
      case NWCornerCell(edge) => {
        g.setFill(edge.style.color)
        n(edge.style)
        w(edge.style)
      }
      case SECornerCell(edge) => {
        g.setFill(edge.style.color)
        s(edge.style)
        e(edge.style)
      }
      case SWCornerCell(edge) => {
        g.setFill(edge.style.color)
        s(edge.style)
        w(edge.style)
      }
      case HStraightCell(edge) => {
        g.setFill(edge.style.color)
        e(edge.style)
        w(edge.style)
      }
      case VStraightCell(edge) => {
        g.setFill(edge.style.color)
        n(edge.style)
        s(edge.style)
      }
    }
  }
}