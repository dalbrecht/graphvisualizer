/* Copyright 2014 Dennis Albrecht
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ui.javafx

import java.{ util => ju }

import scala.collection.JavaConversions.asScalaBuffer
import scala.collection.JavaConversions.bufferAsJavaList
import scala.collection.mutable.HashMap
import scala.math.min

import cells.GraphTableCell
import graph.Graph
import graph.model.Cell
import graph.model.GridColumn
import graph.model.GridRow
import javafx.beans.value.ObservableValue
import javafx.collections.ListChangeListener
import javafx.collections.ListChangeListener.Change
import javafx.collections.ObservableList
import javafx.scene.control.TableCell
import javafx.scene.control.TableColumn
import javafx.scene.control.TableColumn.CellDataFeatures
import javafx.stage.Stage
import javafx.util.Callback
import observable.ObservableGridCell
import observable.ObservableGridColumnList

private[javafx] class ColumnFactory[R, C](table: ObservableList[TableColumn[GridRow[R, C], _]], columns: ObservableGridColumnList[R, C], navigator: Graph[_, _] => Unit, register: Stage => Unit) extends ListChangeListener[GridColumn[R, C]] {
  private val _cache = new HashMap[TableColumn[GridRow[R, C], _], (Option[C], Int)]()
  private var _active = false
  def activate(): Unit = {
    _active = true
    copy(columns, table)
    columns.addListener(this)
  }
  def deactivate(): Unit = {
    _active = false
    columns.removeListener(this)
  }
  def onChanged(change: Change[_ <: GridColumn[R, C]]): Unit = {
    if (columns != change.getList) {
      change.getList.removeListener(this)
    } else if (_active) {
      change.reset
      var success = true
      while (change.next && success) {
        success = success && step(change)
      }
      if (!success)
        copy(columns, table)
    }
  }
  private def step(change: Change[_ <: GridColumn[R, C]]): Boolean = {
    (change.wasPermutated, change.wasUpdated, change.wasReplaced, change.wasRemoved, change.wasAdded) match {
      case (true, _, _, _, _) => {
        val addressedElements = (change.getFrom until change.getTo).toSeq.map(i => (change.getPermutation(i), table.get(i)))
        addressedElements.foreach(ae => table.set(ae._1, ae._2))
        true
      }
      case (_, true, _, _, _) => {
        for (index <- change.getFrom until change.getTo) { // TODO: reference to columns!! dangerous!!
          _cache.remove(table.set(index, createTableColumn({ val col = columns.get(index); (col.column, col.subCol) })))
        }
        true
      }
      case (_, _, true, _, _) => {
        val count = min(change.getRemovedSize, change.getAddedSize)
        val from = change.getFrom
        replaceRange(from, count, table, change.getRemoved.map(r => (r.column, r.subCol)), change.getAddedSubList.view(0, count).map(r => (r.column, r.subCol))) &&
          ((change.getRemovedSize == change.getAddedSize) ||
            {
              if (change.getRemovedSize < change.getAddedSize) {
                insertRange(columns.subList(from + count, change.getTo), from + count, table)
                true
              } else {
                deleteRange(change.getFrom + change.getAddedSize, change.getRemovedSize - change.getAddedSize, table, change.getRemoved.drop(change.getAddedSize).map(r => (r.column, r.subCol)))
              }
            })
      }
      case (_, _, _, true, _) => {
        deleteRange(change.getFrom, change.getRemovedSize, table, change.getRemoved.map(r => (r.column, r.subCol)))
      }
      case (_, _, _, _, true) => {
        insertRange(change.getAddedSubList.map(_.asInstanceOf[GridColumn[R, C]]), change.getFrom, table)
        true
      }
      case (_, _, _, _, _) => true
    }
  }
  private def copy(src: ObservableList[GridColumn[R, C]], trg: ObservableList[TableColumn[GridRow[R, C], _]]): Unit = {
    _cache.clear
    trg.clear
    src.foreach(c => trg.add(createTableColumn(c.column, c.subCol)))
  }
  private def insertRange(src: ju.List[GridColumn[R, C]], pos: Int, trg: ObservableList[TableColumn[GridRow[R, C], _]]): Unit = {
    trg.addAll(pos, src.map(c => createTableColumn((c.column, c.subCol))))
  }
  private def deleteRange(from: Int, count: Int, lst: ObservableList[TableColumn[GridRow[R, C], _]], cmp: Seq[(Option[C], Int)]): Boolean = {
    var success = true
    for (index <- 0 until count) {
      if (success) {
        val nextEl = _cache(lst.get(from))
        val toDelete = cmp(index)
        if (nextEl == toDelete) {
          _cache.remove(lst.remove(from))
        } else {
          success = false
        }
      }
    }
    success
  }
  private def replaceRange(from: Int, count: Int, lst: ObservableList[TableColumn[GridRow[R, C], _]], cmp: Seq[(Option[C], Int)], by: Seq[(Option[C], Int)]): Boolean = {
    var success = true
    for (index <- 0 until count) {
      if (success) {
        val pos = from + index
        val nextEl = _cache(lst.get(pos))
        val toDelete = cmp(index)
        if (nextEl == toDelete) {
          _cache.remove(lst.set(pos, createTableColumn(by(index))))
        } else {
          success = false
        }
      }
    }
    success
  }
  private def createTableColumn(id: (Option[C], Int)): TableColumn[GridRow[R, C], Cell[R, C]] = {
    val column = new TableColumn[GridRow[R, C], Cell[R, C]](if (id._2 == 0) id._1.get.toString else "")
    _cache.put(column, id)
    column.setSortable(false)
    if (id._2 == 0) {
      column.setPrefWidth(100)
    } else {
      column.setResizable(false)
      column.setMinWidth(GraphTableCell.SingleEdgeWidth)
      column.setMaxWidth(GraphTableCell.SingleEdgeWidth)
    }
    column.setCellFactory(new Callback[TableColumn[GridRow[R, C], Cell[R, C]], TableCell[GridRow[R, C], Cell[R, C]]] {
      def call(col: TableColumn[GridRow[R, C], Cell[R, C]]): TableCell[GridRow[R, C], Cell[R, C]] = {
        new GraphTableCell(id, navigator, register: Stage => Unit)
      }
    })
    column.setCellValueFactory(new Callback[CellDataFeatures[GridRow[R, C], Cell[R, C]], ObservableValue[Cell[R, C]]] {
      def call(feature: CellDataFeatures[GridRow[R, C], Cell[R, C]]): ObservableValue[Cell[R, C]] = {
        new ObservableGridCell(feature.getValue.gridCell(id._1, id._2))
      }
    })
    column
  }
}