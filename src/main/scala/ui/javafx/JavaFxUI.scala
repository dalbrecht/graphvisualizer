/* Copyright 2014 Dennis Albrecht
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ui.javafx

import java.util.concurrent.Semaphore

import scala.collection.mutable.Buffer
import scala.collection.mutable.Queue
import scala.collection.mutable.SynchronizedQueue

import graph.Graph
import javafx.application.Application
import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.input.MouseEvent
import javafx.scene.layout.BorderPane
import javafx.scene.layout.HBox
import javafx.stage.Stage
import javafx.stage.WindowEvent

/**
 * Main entry point to launch the JavaFX-UI.
 * The call will (in contrast to the JavaFX-launch-call) return as soon as the UI is shown.
 */
object JavaFxUI {
  private val _queue: Queue[Graph[_, _]] = new SynchronizedQueue
  private val _lock: Semaphore = new Semaphore(1)
  def visualize(graph: Graph[_, _]): Unit = {
    _queue += graph
    _lock.acquireUninterruptibly
    new Thread(new Runnable {
      def run(): Unit = {
        Application.launch(classOf[JavaFxUI])
      }
    }).start
    _lock.acquireUninterruptibly
    _lock.release
  }
}

class JavaFxUI extends Application {
  private val _root = JavaFxUI._queue.dequeue
  private var _released = false
  private var _layer: List[Layer[_, _]] = Nil
  private val _level = new HBox
  private val _pane = new BorderPane { setTop(_level) }
  private var _stage: Stage = _
  private val _infoWindows = Buffer[Stage]()
  def start(stage: Stage): Unit = {
    _stage = stage
    stage.setScene(new Scene(_pane))
    addLayer(_root)
    stage.show
    if (!_released) {
      synchronized {
        if (!_released) {
          _released = true
          JavaFxUI._lock.release
        }
      }
    }
    stage.addEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, new EventHandler[WindowEvent] {
      def handle(event: WindowEvent): Unit = {
        _infoWindows.foreach(_.close)
      }
    })
  }
  private def registerInfoWindow(info: Stage): Unit = {
    _infoWindows.synchronized { _infoWindows += info }
  }
  private def addLayer(graph: Graph[_, _]): Unit = {
    _layer.headOption.map(_.deactivate)
    val layer = new Layer(graph, addLayer, _pane.setCenter, registerInfoWindow)
    _layer ::= layer
    val button = new Button(layer.name) {
      addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler[MouseEvent] {
        def handle(event: MouseEvent): Unit = {
          changeToLayer(layer)
        }
      })
    }
    _level.getChildren.add(button)
    changeToLayer(layer)
  }
  private def changeToLayer(layer: Layer[_, _]): Unit = {
    if (_layer.contains(layer)) {
      _layer.head.deactivate
      val pos = _layer.indexOf(layer)
      if (pos > 0) {
        _layer = _layer.drop(pos)
        val length = _level.getChildren.size
        _level.getChildren.remove(length - pos, length)
      }
      _stage.setTitle(_layer.reverse.map(_.name).mkString("GraphVisualizer: ", " > ", ""))
      _layer.head.activate
    }
  }
}