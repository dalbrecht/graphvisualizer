/* Copyright 2014 Dennis Albrecht
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ui.javafx
package observable

import java.util.AbstractList
import java.util.Collection

import scala.collection.mutable.Buffer
import scala.collection.mutable.HashMap

import com.sun.javafx.collections.NonIterableChange.SimpleAddChange

import graph.model.CellUpdate
import graph.model.ColumnAddition
import graph.model.Grid
import graph.model.GridChange
import graph.model.GridColumn
import graph.model.GridRow
import graph.model.RowAddition
import javafx.beans.InvalidationListener
import javafx.collections.ListChangeListener
import javafx.collections.ObservableList
import util.Observable
import util.Observer

abstract class ObservableListAdapter[L, O](observable: Observable[O]) extends AbstractList[L] with ObservableList[L] with Observer[O] {
  observable.register(this)
  private val _listener: Buffer[ListChangeListener[_ >: L]] = Buffer()

  def addAll(els: L*): Boolean = throw new UnsupportedOperationException()
  def remove(index1: Int, index2: Int): Unit = throw new UnsupportedOperationException()
  def removeAll(els: L*): Boolean = throw new UnsupportedOperationException()
  def retainAll(els: L*): Boolean = throw new UnsupportedOperationException()
  def setAll(els: L*): Boolean = throw new UnsupportedOperationException()
  def setAll(els: Collection[_ <: L]): Boolean = throw new UnsupportedOperationException()
  // no storage of InvalidationListeners because this implementation does not get invalidated
  def addListener(listener: InvalidationListener): Unit = ()
  def removeListener(listener: InvalidationListener): Unit = ()
  def addListener(listener: ListChangeListener[_ >: L]): Unit = {
    _listener.synchronized { _listener += listener }
  }
  def removeListener(listener: ListChangeListener[_ >: L]): Unit = {
    _listener.synchronized { _listener -= listener }
  }

  protected def fire(change: ListChangeListener.Change[L]): Unit = _listener.foreach {
    l =>
      try {
        l.onChanged(change)
      } catch {
        case t: Exception => Console.err.println(s"$l $t")
      }
  }
}

class ObservableGridRowList[R, C](grid: Grid[R, C]) extends ObservableListAdapter[GridRow[R, C], GridChange[R, C]](grid) {
  private val _cache: HashMap[(Option[R], Int), GridRow[R, C]] = new HashMap()

  def get(index: Int): GridRow[R, C] = getRow(grid.rows.apply(index))
  def size(): Int = grid.rows.length
  def update(observable: Observable[GridChange[R, C]], change: GridChange[R, C]): Unit = {
    if (grid != observable)
      observable.unregister(this)
    else {
      change match {
        case CellUpdate(r, s, _, _, _) =>
        case RowAddition(i, _, _) => fire(new SimpleAddChange(i, i + 1, this))
        case ColumnAddition(_, _, _) =>
      }
    }
  }

  private def getRow(index: (Option[R], Int)): GridRow[R, C] = {
    if (_cache.isDefinedAt(index))
      _cache(index)
    else
      _cache.synchronized {
        if (_cache.isDefinedAt(index))
          _cache(index)
        else {
          val row = grid.getRow(index._1, index._2)
          _cache put (index, row)
          row
        }
      }
  }
}

class ObservableGridColumnList[R, C](grid: Grid[R, C]) extends ObservableListAdapter[GridColumn[R, C], GridChange[R, C]](grid) {
  private val _cache: HashMap[(Option[C], Int), GridColumn[R, C]] = new HashMap()

  def get(index: Int): GridColumn[R, C] = getColumn(grid.columns.apply(index))
  def size(): Int = grid.columns.length
  def update(observable: Observable[GridChange[R, C]], change: GridChange[R, C]): Unit = {
    if (grid != observable)
      observable.unregister(this)
    else {
      change match {
        case CellUpdate(_, _, _, _, _) =>
        case RowAddition(_, _, _) =>
        case ColumnAddition(i, _, _) => fire(new SimpleAddChange(i, i + 1, this))
      }
    }
  }

  private def getColumn(index: (Option[C], Int)): GridColumn[R, C] = {
    if (_cache.isDefinedAt(index))
      _cache(index)
    else
      _cache.synchronized {
        if (_cache.isDefinedAt(index))
          _cache(index)
        else {
          val col = grid.getColumn(index._1, index._2)
          _cache put (index, col)
          col
        }
      }
  }
}