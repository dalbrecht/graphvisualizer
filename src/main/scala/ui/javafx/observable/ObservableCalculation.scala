/* Copyright 2014 Dennis Albrecht
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ui.javafx.observable

import com.sun.javafx.collections.ImmutableObservableList

import javafx.beans.binding.ObjectBinding
import javafx.beans.value.ObservableValue
import javafx.collections.FXCollections
import javafx.collections.ObservableList

/**
 * More generic version of javafx's calculation opportunity with {@link NumberExpression}s.
 */
object ObservableCalculation {
  /**
   * for unary calculations (like not) or binary (or higher order) calculations with mostly constants
   */
  def calc[P, Res](f: P => Res, param: ObservableValue[P]): ObservableValue[Res] = {
    new ObjectBindingBase[Res](param) {
      def computeValue(): Res = {
        f(param.getValue)
      }
    }
  }
  /**
   * for binary calculations (like max) that are not offered by {@link NumberExpression}
   */
  def calc[P1, P2, Res](f: (P1, P2) => Res, param1: ObservableValue[P1], param2: ObservableValue[P2]): ObservableValue[Res] = {
    new ObjectBindingBase[Res](param1, param2) {
      def computeValue(): Res = {
        f(param1.getValue, param2.getValue)
      }
    }
  }
  /**
   * for ternary calculations (like if-then-else)
   */
  def calc[P1, P2, P3, Res](f: (P1, P2, P3) => Res, param1: ObservableValue[P1], param2: ObservableValue[P2], param3: ObservableValue[P3]): ObservableValue[Res] = {
    new ObjectBindingBase[Res](param1, param2, param3) {
      def computeValue(): Res = {
        f(param1.getValue, param2.getValue, param3.getValue)
      }
    }
  }
}

private abstract class ObjectBindingBase[T](deps: ObservableValue[_]*) extends ObjectBinding[T] {
  super.bind(deps: _*)
  override def getDependencies(): ObservableList[_] = {
    if (deps.length == 1) FXCollections.singletonObservableList(deps.head) else new ImmutableObservableList(deps: _*)
  }
  override def dispose(): Unit = {
    super.unbind(deps: _*)
  }
}