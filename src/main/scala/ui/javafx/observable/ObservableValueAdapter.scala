/* Copyright 2014 Dennis Albrecht
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ui.javafx
package observable

import scala.collection.mutable.Buffer
import scala.language.existentials

import graph.model.Cell
import graph.model.CellUpdate
import graph.model.GridCell
import graph.model.GridChange
import javafx.beans.InvalidationListener
import javafx.beans.property.ReadOnlyObjectProperty
import javafx.beans.value.ChangeListener
import util.Observable
import util.Observer

abstract class ObservableValueAdapter[V, O](observable: Observable[O]) extends ReadOnlyObjectProperty[V] with Observer[O] {
  observable.register(this)
  private val _listener: Buffer[ChangeListener[_ >: V]] = Buffer()

  def addListener(listener: ChangeListener[_ >: V]): Unit = {
    _listener.synchronized { _listener += listener }
  }
  def removeListener(listener: ChangeListener[_ >: V]): Unit = {
    _listener.synchronized { _listener -= listener }
  }
  def addListener(listener: InvalidationListener): Unit = ()
  def removeListener(listener: InvalidationListener): Unit = ()
  def getBean(): Object = null
  def getName(): String = ""
  protected def fire(oldVal: V, newVal: V): Unit = {
    _listener.synchronized { _listener.foreach(_.changed(this, oldVal, newVal)) }
  }
}

class ObservableGridCell[R, C](cell: GridCell[R, C]) extends ObservableValueAdapter[Cell[R, C], GridChange[R, C]](cell) {
  private var _value: Cell[R, C] = cell.getValue
  fire(null, _value)

  def update(observable: Observable[GridChange[R, C]], change: GridChange[R, C]): Unit = {
    if (observable != cell) {
      observable.unregister(this)
    } else {
      change match {
        case CellUpdate(cell.row, cell.subRow, cell.column, cell.subCol, update) =>
          val old = _value
          _value = update
          fire(old, _value)
        case _ =>
      }
    }
  }
  override def addListener(listener: ChangeListener[_ >: Cell[R, C]]): Unit = {
    super.addListener(listener)
    listener.changed(this, null, _value)
  }
  def get(): graph.model.Cell[R, C] = _value
}