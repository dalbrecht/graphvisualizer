/* Copyright 2014 Dennis Albrecht
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ui.javafx
package observable

import java.{ util => ju }
import java.util.Collection
import java.util.Iterator
import java.util.ListIterator
import java.util.concurrent.Callable
import java.util.concurrent.ExecutionException
import java.util.concurrent.FutureTask

import javafx.application.Platform
import javafx.beans.InvalidationListener
import javafx.collections.ListChangeListener
import javafx.collections.ObservableList

private[javafx] class ObservableListProxy[E](observable: ObservableList[E]) extends ObservableList[E] {
  def size(): Int = runLater(() => observable.size)
  def isEmpty(): Boolean = runLater(() => observable.isEmpty)
  def contains(o: Any): Boolean = runLater(() => observable.contains(o))
  def iterator(): Iterator[E] = runLater(() => observable.iterator)
  def toArray(): Array[AnyRef] = runLater(() => observable.toArray)
  def toArray[T](a: Array[T with AnyRef]): Array[T with AnyRef] = runLater(() => observable.toArray[T](a))
  def add(e: E): Boolean = runLater(() => observable.add(e))
  def remove(o: Any): Boolean = runLater(() => observable.remove(o))
  def containsAll(c: Collection[_]): Boolean = runLater(() => observable.containsAll(c))
  def addAll(c: Collection[_ <: E]): Boolean = runLater(() => observable.asInstanceOf[ju.List[E]].addAll(c))
  def addAll(index: Int, c: Collection[_ <: E]): Boolean = runLater(() => observable.addAll(index, c))
  def removeAll(c: Collection[_]): Boolean = runLater(() => observable.asInstanceOf[ju.List[E]].removeAll(c))
  def retainAll(c: Collection[_]): Boolean = runLater(() => observable.asInstanceOf[ju.List[E]].retainAll(c))
  def clear(): Unit = runLater(() => observable.clear)
  def get(index: Int): E = runLater(() => observable.get(index))
  def set(index: Int, element: E): E = runLater(() => observable.set(index, element))
  def add(index: Int, element: E): Unit = runLater(() => observable.add(index, element))
  def remove(index: Int): E = runLater(() => observable.remove(index))
  def indexOf(o: Any): Int = runLater(() => observable.indexOf(o))
  def lastIndexOf(o: Any): Int = runLater(() => observable.lastIndexOf(o))
  def listIterator(): ListIterator[E] = runLater(() => observable.listIterator)
  def listIterator(index: Int): ListIterator[E] = runLater(() => observable.listIterator(index))
  def subList(fromIndex: Int, toIndex: Int): ju.List[E] = runLater(() => observable.subList(fromIndex, toIndex))
  def addListener(listener: InvalidationListener): Unit = runLater(() => observable.addListener(listener))
  def removeListener(listener: InvalidationListener): Unit = runLater(() => observable.removeListener(listener))
  def addListener(listener: ListChangeListener[_ >: E]): Unit = runLater(() => observable.addListener(listener))
  def removeListener(listener: ListChangeListener[_ >: E]): Unit = runLater(() => observable.removeListener(listener))
  def remove(fromIndex: Int, toIndex: Int): Unit = runLater(() => observable.remove(fromIndex, toIndex))
  def addAll(elements: E*): Boolean = runLater(() => observable.addAll(elements: _*))
  def removeAll(elements: E*): Boolean = runLater(() => observable.removeAll(elements: _*))
  def retainAll(elements: E*): Boolean = runLater(() => observable.retainAll(elements: _*))
  def setAll(elements: E*): Boolean = runLater(() => observable.setAll(elements: _*))
  def setAll(elements: Collection[_ <: E]): Boolean = runLater(() => observable.setAll(elements))
  private def runLater(action: () => Unit): Unit = {
    if (Platform.isFxApplicationThread())
      action()
    else
      Platform.runLater(new Runnable { def run(): Unit = action() })
  }
  private def runLater[R](action: () => R): R = {
    if (Platform.isFxApplicationThread())
      action()
    else {
      val task = new FutureTask(new Callable[R] { def call(): R = action() })
      Platform.runLater(task)
      try {
        task.get()
      } catch { case e: ExecutionException => println(s"I was here\n${e.getCause()}\n${e.getCause().getStackTrace().mkString("\n")}"); throw e }
    }
  }
}